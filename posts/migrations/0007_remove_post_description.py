# Generated by Django 4.0.3 on 2022-03-07 18:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('posts', '0006_rename_created_comment_created_at'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='post',
            name='description',
        ),
    ]
